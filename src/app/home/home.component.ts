import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Observable, Subscription} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  intervalSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    // every 1s a new event will be emitted
    // this.intervalSubscription = interval(1000).subscribe(count => {
    //   console.log('Count: ', count);
    // })
    const customIntervalObservable = new Observable((observer) => {
      let count = 0;
      setInterval(() => {
        observer.next(count);

        if(count === 2) {
          observer.complete();
        }

        if(count > 3) {
          observer.error(new Error('Count is greater 3'));
        }
        count++;
      }, 1000);
    });



    this.intervalSubscription = customIntervalObservable
      .pipe(
        filter((data: number) => data % 2 === 0),
        map((data: number) => 'Round: ' + data)
      )
      .subscribe((count => {
        console.log(count);
      }), error => {
        console.log(error);
      }, () => {
        console.log('Completed!');
      });
  }

  ngOnDestroy() {
    this.intervalSubscription.unsubscribe();
  }

}
